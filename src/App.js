import './App.css';
import React, { useState, useEffect } from 'react';

import { Contador } from './Contador';
import { Item } from './Item';
import { Lista } from './Lista';
import { Boton } from './Boton';
import { Buscador } from './Buscador';

/**
 * Para poder utilizar correctamente, se deben precargar las tareas desde consola,
 * por tanto es necesario crear/ejecutar en la consola:
 * 
 * 
  const tareasDefault = [
  { id: 1, text: 'Tarea 1: Lavar y planchar ropa.', completed: false },
  { id: 2, text: 'Tarea 2: Comprar viveres.', completed: false },
  { id: 3, text: 'Tarea 3: Regar plantas.', completed: true },
  { id: 4, text: 'Tarea 4: Cambiar focos.', completed: false },
  { id: 5, text: 'Tarea 5: Ir a trabajar.', completed: false },
  { id: 6, text: 'Tarea 6: Limpiar perritos.', completed: false },
  { id: 7, text: 'Tarea 7: Dormir.', completed: false },
  { id: 8, text: 'Tarea 8: Regresar a casa.', completed: false },
  { id: 9, text: 'Tarea 9: Correr 10km.', completed: false },
 
];

localStorage.setItem('TAREAS_V1', JSON.stringify(tareasDefault));

 * posteriormente se recarga la pagina
 * 
 */

function App() {
  /** P E R S I S T E N C I A  D E  D A T O S */
  const localStorageTareas = localStorage.getItem('TAREAS_V1');

  let parsedTareas;

  //En caso que el usuario no tenga tareas registradas
  if (!localStorageTareas) {
    localStorage.setItem('TAREAS_V1', JSON.stringify([]));
    //console.log('no hay localstorage');
  } else {
    parsedTareas = JSON.parse(localStorageTareas);
    //console.log('si hay localstorage');
  }

  /** G U A R D A R T A R E A S */
  const guardarTareas = (newTarea) => {
    const stringTareas = JSON.stringify(newTarea);
    localStorage.setItem('TAREAS_V1', stringTareas);
    setTareas(newTarea);
  };

  /** C O N T A D O R */
  //crear estado del contador

  const [tareas, setTareas] = React.useState(parsedTareas);

  //crear variables que almacenan las tareas
  const completdTareas = tareas.filter((tarea) => !!tarea.completed).length;
  //Crear variable que que almacena total de tareas
  const totalTareas = tareas.length;

  //** Completar tarea  */
  const completarTarea = (text) => {
    const TareaIndex = tareas.findIndex((tarea) => tarea.text === text);
    const nuevasTareas = [...tareas];
    nuevasTareas[TareaIndex].completed = true;
    setTareas(nuevasTareas);
  };

  //** B U S C A D O R */
  const [search, setSearch] = useState('');

  //función condicional de busqueda
  let results = !search
    ? tareas
    : tareas.filter((dato) =>
        dato.text.toLowerCase().includes(search.toLocaleLowerCase())
      );

  //Eliminar tarea
  const eliminarTarea = (taskId) => {
    const tareaIndex = tareas.findIndex((tarea) => tarea.id == taskId);
    const nuevasTareas = [...tareas];
    nuevasTareas.splice(tareaIndex, 1);
    guardarTareas(nuevasTareas);
  };

  return (
    <React.Fragment>
      <Contador tare={completdTareas} total={totalTareas} />

      <Buscador search={search} setSearch={setSearch} />
      <Lista>
        {results.map((tarea, index) => (
          <Item
            text={tarea.text}
            numero={'0' + (index + 1)}
            key={tarea.text}
            completed={tarea.completed}
            id={tarea.id}
            eliminarTarea={eliminarTarea}
            onComplete={() => completarTarea(tarea.text)}
          />
        ))}
      </Lista>

      <Boton />
    </React.Fragment>
  );
}

export default App;
