import React from "react";
import "./Buscador.css";

function Buscador({ search, setSearch }) {
  //Estado
  const [buscandoValor, setBuscandoValor] = React.useState("");
  const buscando = (event) => {
    console.log(event.target.value);
    setBuscandoValor(event.target.value);
  };

  const searcher = (e) => {
    setSearch(e.target.value);
  };

  return (
    <section className="section-search">
      <input
        type={"text"}
        onChange={searcher}
        className="form-search"
        value={search}
        placeholder="Buscar una tarea..."
      />
    </section>
  );
}

export { Buscador };
