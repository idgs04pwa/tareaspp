import React from 'react';
import './Lista.css';



function Lista(props) {
    return (
        <section>
            <ul className="lista-principal">
                {props.children}
            </ul>
        </section>
    );
}


export { Lista }