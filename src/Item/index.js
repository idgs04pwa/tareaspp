import React from 'react';
import './Item.css';

function Item(props, onComplete, completed) {
  const deleting = () => {
    alert('Eliminaste la tarea: ' + props.text);
    props.eliminarTarea(props.id);
  };

  return (
    <li className='item-lista'>
      <div className='justify-between'>
        <span className='check-icon' onClick={props.onComplete}>
          {' '}
          <strong>✓</strong>{' '}
        </span>

        <p className={props.completed ? 'completado' : 'p-text'}>
          <span className='p-number-index'>{props.numero} </span>
          {props.text}
        </p>

        <span className='cross-icon' onClick={deleting}>
          <strong>X</strong>
        </span>
      </div>
    </li>
  );
}

export { Item };
