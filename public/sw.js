
//imports

if( 'undefined' === typeof window){
    importScripts('js/sw-utils.js');
 }
//Configuracion del service worker

const ESTATICO_CACHE = 'static-v1'
const DINAMICO_CACHE = 'dinamico-v1'
const INMUTABLE_CACHE = 'inmutable-v1'

//Los archivos minimos basicos para que el sitio funcione
const APP_SHELL = [ 
    '/',
    'index.html',
    'logo192.png',
    'logo512.png',
    'manifest.json',
    'js/app.js',
    'js/sw-utils.js',
    'src/index.js',
    'src/index.css',
    'src/package.json',

]

const APP_SHELL_INMUTABLE = [
    'https://fonts.googleapis.com/css2?family=Red+Hat+Display:ital,wght@0,300;0,400;0,500;1,500;1,600&display=swap',
]

//Proceso de instalacion
self.addEventListener('install', event=>{
    const cacheStatic = caches.open(ESTATICO_CACHE).then(cache=>
    cache.addAll(APP_SHELL))
    const cacheInmutable = caches.open(INMUTABLE_CACHE).then(cache=> cache.addAll(APP_SHELL_INMUTABLE))

    event.waitUntil(Promise.all[cacheStatic, cacheInmutable])
})

//Proceso de activavion
self.addEventListener('activate', event=>{
    const respuesta = caches.keys().then(keys=>{
        keys.forEach(key=>{
            if(key != ESTATICO_CACHE && key.includes('static')){
                return caches.delete(key)
            }
        })
    })
   event.waitUntil(respuesta) 
})



//Estrategia de cache
self.addEventListener('fetch', event =>{
    const respuesta = caches.match(event.request).then(res=>{
        if(res){
            console.log('returning res')
            return res
        }else{
            return fetch(event.request).then(newRes => {
                //primero
                return actualizaCacheDinamico(DINAMICO_CACHE, event.request, newRes)
            })
        }
    })

    event.respondWith(respuesta)
})